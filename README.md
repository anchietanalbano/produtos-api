# API de Produtos

Esta é uma API para gerenciar produtos (CRUD) desenvolvido com Spring Boot.

<p align="center">
 <img src="https://img.shields.io/static/v1?label=Tipo&message=API&color=8257E5&labelColor=000000" alt="Api" />
</p>

## Organização de Diretórios


- **controllers**: Contém os controladores REST que definem os endpoints.
- **dtos**: Armazena os objetos de transferência de dados (DTOs) usados para entrada/saída.
- **models**: Contém as entidades JPA que representam os produtos.
- **repository**: Contém a interface do repositório JPA para acessar os dados dos produtos.

## Tecnologias

- [Spring Boot](https://spring.io/projects/spring-boot)
- [Spring MVC](https://docs.spring.io/spring-framework/reference/web/webmvc.html)
- [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
- [SpringDoc OpenAPI 3](https://springdoc.org/v2/#spring-webflux-support)
- [Mysql](https://dev.mysql.com/downloads/)

## Práticas adotadas

- SOLID, DRY, YAGNI, KISS
- API REST
- Consultas com Spring Data JPA
- Injeção de Dependências
- Geração automática do Swagger com a OpenAPI 3

## Como Executar


## Endpoints

### 1. Criar um Produto

- **URL**: `POST /products`
- **Descrição**: Cria um novo produto com base nos dados fornecidos.
- **Resposta (JSON)**:
  ```json
  {
    "name": "Produto B",
    "value": 79.99
  }

### 2. Recuperar Todos os Produtos

- **URL**: `GET /products`
- **Descrição**: Retorna uma lista de todos os produtos.
- **Resposta (JSON)**:
  ```json
  [ {
    "idProduct": 1,
    "name": "Produto A",
    "value": 59.99
    },
    {
    "idProduct": 2,
    "name": "Produto B",
    "value": 79.99
    }
  ]

### 3. Recuperar um Produto por ID

- **URL**: `GET /products/{id}`
- **Descrição**: Retorna os detalhes de um produto específico com base no ID.
- **Resposta (JSON)**:
  ```json
  {
  "idProduct": 1,
  "name": "Produto A",
  "value": 59.99
  }

### 4. Atualizar um Produto por ID

- **URL**: `PUT /products/{id}`
- **Descrição**: Atualiza os detalhes de um produto específico com base no ID.
- **Requisição (JSON)**:
  ```json
  {
  "idProduct": 1,
  "name": "Produto AB",
  "value": 59.99
  }
- **Resposta (JSON)** :
  ```json
  {
  "idProduct": 1,
  "name": "Produto AB",
  "value": 59.99
  }

### 5. Excluir um Produto por ID

- **URL**: `DELETE /products/{id}`
- **Descrição**: Exclui um produto específico com base no ID.
- **Resposta (Texte)**: "Produto deletado com sucesso."

### Configuração
- Configure o banco de dados no arquivo `application.properties`.

